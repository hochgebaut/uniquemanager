package de.minecraftunique.dersnatch.database;

import com.mongodb.ConnectionString;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;

public class MongoManager {

    private MongoClient client;
    private MongoDatabase database;

    private MongoCollection<Document> playerInfo;

    public MongoManager() {}

    public void connect() {
        this.client = MongoClients.create(new ConnectionString("mongodb://DerSnatch:arschgesicht99@ds135956.mlab.com:35956/minecraftunique"));
        this.database = this.client.getDatabase("minecraftunique");
        this.playerInfo = this.database.getCollection("PlayerInfo");
    }

    public MongoCollection<Document> getPlayerInfo() {
        System.out.println("getPlayerInfo :D");
        return playerInfo;
    }
}
