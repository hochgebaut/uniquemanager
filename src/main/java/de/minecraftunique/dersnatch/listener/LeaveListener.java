package de.minecraftunique.dersnatch.listener;

import de.minecraftunique.dersnatch.api.UniquePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveListener implements Listener {

    @EventHandler
    public void on(PlayerQuitEvent event) {
        new UniquePlayer(event.getPlayer()).savePlayer();
    }
}
