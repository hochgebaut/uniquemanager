package de.minecraftunique.dersnatch.listener;

import de.minecraftunique.dersnatch.MinecraftUniqueServerAPI;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class DamageListener implements Listener {

    @EventHandler
    public void on(EntityDamageByEntityEvent event) {
        if (MinecraftUniqueServerAPI.getInstance().isLobbyServer()) {
            event.setCancelled(true);
        }
    }

}
