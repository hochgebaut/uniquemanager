package de.minecraftunique.dersnatch.listener;

import de.minecraftunique.dersnatch.util.InventoryUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener {

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        player.sendMessage("§eDein Charakter wird auf den neusten Stand gebracht...!");
        InventoryUtil.loadPlayer(player);
    }

}
