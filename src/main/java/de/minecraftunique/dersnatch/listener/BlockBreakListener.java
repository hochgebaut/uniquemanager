package de.minecraftunique.dersnatch.listener;

import de.minecraftunique.dersnatch.MinecraftUniqueServerAPI;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import static de.minecraftunique.dersnatch.api.UniqueMessage.DENY_BREAK;
import static de.minecraftunique.dersnatch.api.UniqueMessage.DENY_BUILD;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void on(BlockBreakEvent event) {
        if (MinecraftUniqueServerAPI.getInstance().isLobbyServer()) {
            if (!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
                event.getPlayer().sendMessage(DENY_BREAK.m());
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void on(BlockPlaceEvent event) {
        if (MinecraftUniqueServerAPI.getInstance().isLobbyServer()) {
            if (!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
                event.getPlayer().sendMessage(DENY_BUILD.m());
                event.setCancelled(true);
            }
        }
    }

}
