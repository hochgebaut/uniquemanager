package de.minecraftunique.dersnatch.commands;

import de.minecraftunique.dersnatch.util.InventoryUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Inventory_Command implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player player = (Player) commandSender;
        if (args.length == 1) {
            player.getInventory().setContents(InventoryUtil.itemStackArrayFromBase64("rO0ABXcEAAAAKXBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwv"));
            return true;
        }
        InventoryUtil.loadPlayer(player);
        return true;
    }
}
