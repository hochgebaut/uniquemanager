package de.minecraftunique.dersnatch.commands;

import de.minecraftunique.dersnatch.api.guild.IGuild;
import de.minecraftunique.dersnatch.api.guild.IGuildForm;
import de.minecraftunique.dersnatch.api.guild.IGuildUser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.minecraftunique.dersnatch.api.UniqueMessage.IGUILD_DENY_CREATE;

public class Guild_Command implements CommandExecutor {

    /**
     * /guild
     * /guild create <Guild-Name> <Kreis|Quadrat> <Größe>
     * /guild invite <Player-Name>
     * /guild kick <Player-Name>
     * /guild leave
     * /guild request <Guild-Name>
     */

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player player = (Player) commandSender;
        IGuildUser iGuildUser = new IGuildUser(player);
        if (args[0].equalsIgnoreCase("create")) {
            if (iGuildUser.canCreateGuild()) {
                if (args.length == 4) {
                    IGuild iGuild = iGuildUser.createGuild(args[1], IGuildForm.QUADRAT, 80);

                    return true;
                }
                player.sendMessage("§cBenutze: §7/guild create <Gilden-Name> <Kreis|Quadrat> <Größe>");
                return true;
            }
            player.sendMessage(IGUILD_DENY_CREATE.m());
        }
        if (args[0].equalsIgnoreCase("invite")) {

        }
        if (args[0].equalsIgnoreCase("kick")) {

        }
        if (args[0].equalsIgnoreCase("request")) {

        }
        if (args[0].equalsIgnoreCase("leave")) {

        }
        if (args[0].equalsIgnoreCase("info")) {

        }
        return true;
    }
}
