package de.minecraftunique.dersnatch.util;

import de.minecraftunique.dersnatch.MinecraftUniqueServerAPI;
import de.minecraftunique.dersnatch.api.UniquePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InventoryUtil {

    public static String[] playerInventoryToBase64(PlayerInventory playerInventory) throws IllegalStateException {
        String content = toBase64(playerInventory);
        String armor = itemStackArrayToBase64(playerInventory.getArmorContents());

        return new String[]{content, armor};
    }

    public static String itemStackArrayToBase64(ItemStack[] items) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            dataOutput.writeInt(items.length);

            for (int i = 0; i < items.length; i++) {
                dataOutput.writeObject(items[i]);
            }

            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    public static String toBase64(Inventory inventory) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            dataOutput.writeInt(inventory.getSize());

            for (int i = 0; i < inventory.getSize(); i++) {
                dataOutput.writeObject(inventory.getItem(i));
            }

            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    public static Inventory fromBase64(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            Inventory inventory = Bukkit.getServer().createInventory(null, dataInput.readInt());

            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }
            dataInput.close();
            return inventory;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }

    public static ItemStack[] itemStackArrayFromBase64(String data) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack[] items = new ItemStack[dataInput.readInt()];

            for (int i = 0; i < items.length; i++) {
                items[i] = (ItemStack) dataInput.readObject();
            }

            dataInput.close();
            return items;
        } catch (Exception e) {
            return null;
        }
    }

    public static void loadPlayer(Player player) {
        long time = System.currentTimeMillis();
        player.sendMessage("§eDein Charakter wird auf den neusten Stand gebracht...!");
        try {
            if (MinecraftUniqueServerAPI.getInstance().isEntdeckerFarmwelt())
                player.teleport(MinecraftUniqueServerAPI.getInstance().getGlobalWorldSpawn());
            UniquePlayer uniquePlayer = new UniquePlayer(player);
            Bukkit.getScheduler().runTaskLaterAsynchronously(MinecraftUniqueServerAPI.getInstance(), () -> {
                if (uniquePlayer.isFirstLogin())
                    player.sendMessage("§bWillkommen! Dies ist dein erster Besuch auf unserem Server");
                player.setHealthScale(20);
                if (uniquePlayer.getHealth() > 20)
                    player.setHealthScale(uniquePlayer.getHealth());
                player.setGameMode(uniquePlayer.getGameMode());
                player.setHealth(uniquePlayer.getHealth());
                player.setFoodLevel(uniquePlayer.getFood());
                player.getInventory().setContents(InventoryUtil.itemStackArrayFromBase64(uniquePlayer.getInventoryContent()));
                player.sendMessage("§aDein Charakter wurde aktualisiert! §7[" + (System.currentTimeMillis() - time)+ "ms]");
            }, 30L);
        } catch (Exception e) {
            player.sendMessage("§cDein Charakter scheint ein Problem zu haben...!\n§7" + e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getFirstItems() {
        List<ItemStack> itemStacks = new ArrayList<>();
        itemStacks.add(new ItemBuilder(Material.LEATHER_HELMET).getItemStack());
        itemStacks.add(new ItemBuilder(Material.LEATHER_CHESTPLATE).getItemStack());
        itemStacks.add(new ItemBuilder(Material.LEATHER_LEGGINGS).getItemStack());
        itemStacks.add(new ItemBuilder(Material.LEATHER_BOOTS).getItemStack());
        return itemStackArrayToBase64(itemStacks.toArray(new ItemStack[0]));
    }
}
