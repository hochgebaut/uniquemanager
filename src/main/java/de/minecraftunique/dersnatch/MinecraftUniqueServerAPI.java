package de.minecraftunique.dersnatch;

import de.dytanic.cloudnet.bridge.CloudServer;
import de.minecraftunique.dersnatch.api.ServerPlayer;
import de.minecraftunique.dersnatch.api.UniquePlayer;
import de.minecraftunique.dersnatch.api.UniqueSchematic;
import de.minecraftunique.dersnatch.commands.Guild_Command;
import de.minecraftunique.dersnatch.commands.Inventory_Command;
import de.minecraftunique.dersnatch.database.MongoManager;
import de.minecraftunique.dersnatch.listener.BlockBreakListener;
import de.minecraftunique.dersnatch.listener.DamageListener;
import de.minecraftunique.dersnatch.listener.LeaveListener;
import de.minecraftunique.dersnatch.listener.LoginListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class MinecraftUniqueServerAPI extends JavaPlugin {

    static MinecraftUniqueServerAPI instance;
    public MongoManager mongoManager;

    private boolean isLobby;
    private boolean isEntdeckerFarmwelt;
    private Location globalWorldSpawn;

    @Override
    public void onEnable() {
        instance = this;
        this.mongoManager = new MongoManager();
        connectMongoDB();
        init();
    }

    @Override
    public void onDisable() {
        initD();
    }

    private void init() {
        initServers();
        registerChannels();
        registerListener();
        registerCommands();
    }

    private void initServers() {
        globalWorldSpawn = Bukkit.getWorld("world").getSpawnLocation();
        if (CloudServer.getInstance().getGroupData().getName().startsWith("Lobby")) {
            isLobby = true;
        }
        if (CloudServer.getInstance().getGroupData().getName().startsWith("Entdecker-Farmwelt")) {
            isEntdeckerFarmwelt = true;
            new UniqueSchematic(globalWorldSpawn, new File("/Server-Netzwerk/Schematics/Entdecker-Farmwelten-Spawn.schematic"));
        }
    }

    private void initD() {

    }

    private void registerChannels() {
        getCommand("guild").setExecutor(new Guild_Command());
        getCommand("inv").setExecutor(new Inventory_Command());
    }

    private void registerListener() {
        Bukkit.getPluginManager().registerEvents(new LoginListener(), this);
        Bukkit.getPluginManager().registerEvents(new LeaveListener(), this);
        Bukkit.getPluginManager().registerEvents(new BlockBreakListener(), this);
        Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
    }

    private void registerCommands() {

    }

    public static MinecraftUniqueServerAPI getInstance() {
        return instance;
    }

    public MongoManager getMongoManager() {
        return mongoManager;
    }

    private void connectMongoDB() {
        getMongoManager().connect();
    }

    public Location getGlobalWorldSpawn() {
        return globalWorldSpawn;
    }

    public boolean isLobbyServer() {
        return isLobby;
    }

    public boolean isEntdeckerFarmwelt() {
        return isEntdeckerFarmwelt;
    }
}
