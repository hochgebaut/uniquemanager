package de.minecraftunique.dersnatch.api;

public enum UniqueMessage {

    DENY_BUILD("§c§LHEY! §7Hier kannst du nichts bauen!"),
    DENY_BREAK("§c§LHEY! §7Hier kannst du nichts abbauen!"),
    DENY_PERMISSIONS("§c§LHEY! §7Hierzu hast du keine Rechte!"),

    IGUILD_DENY_CREATE("§c§LHEY! §7Du kannst keine Gilde gründen!");

    private String m;

    UniqueMessage(String message) {
        this.m = message;
    }

    public String m() {
        return m;
    }

}
