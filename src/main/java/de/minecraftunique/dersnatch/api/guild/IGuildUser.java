package de.minecraftunique.dersnatch.api.guild;

import org.bukkit.entity.Player;

public class IGuildUser {

    private Player player;
    private IGuild iGuild;

    public IGuildUser(Player player) {
        this.player = player;
    }

    public boolean canCreateGuild() {
        return true;
    }

    public IGuild getiGuild() {
        return iGuild;
    }

    public IGuild createGuild(String name, IGuildForm guildForm, int size) {
        return new IGuild(this.player, name, guildForm, size);
    }
}
