package de.minecraftunique.dersnatch.api;

import com.mongodb.client.model.Filters;
import de.minecraftunique.dersnatch.MinecraftUniqueServerAPI;
import de.minecraftunique.dersnatch.util.InventoryUtil;
import de.minecraftunique.dersnatch.util.uuid.UUIDFetcher;
import org.bson.Document;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.UUID;

public class UniquePlayer {

    private String column_uuid = "UUID";
    private String column_lastName = "Last Name";
    private String column_firstLoginTime = "First Login";
    private String column_lastLoginTime = "Last Login";
    private String column_lastLogoutTime = "Last Logout";
    private String column_onlineTime = "Online Time";
    private String column_inventoryContent = "Inventory Content";
    private String column_playerHealth = "Health";
    private String column_playerFoodLevel = "Food";
    private String column_playerGamemode = "Gamemode";

    public static Player player;
    public static UUID uuid;
    public static String name;
    public static boolean firstLogin;
    public static long firstLoginMillis;
    public static long lastLoginMillis;
    public static long lastLogoutMillis;
    public static long onlineTimeMillis;
    public static String inventoryContent;
    public static double health;
    public static int food;
    public static GameMode gameMode;

    public UniquePlayer(Player player) {
        this.player = player;
        this.uuid = UUIDFetcher.getUUID(player.getName());
        MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInfo().find(new Document("UUID", uuid)).first((document, throwable) -> {
            if (document == null) {
                this.firstLogin = true;
                this.name = player.getName();
                this.firstLoginMillis = System.currentTimeMillis();
                this.lastLoginMillis = System.currentTimeMillis();
                this.lastLogoutMillis = 0;
                this.onlineTimeMillis = 0;
                this.inventoryContent = InventoryUtil.getFirstItems();
                this.health = 20.0;
                this.food = 15;
                this.gameMode = GameMode.SURVIVAL;
                MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInfo().insertOne(new Document("UUID", this.uuid), (aVoid, thro) -> {});
                return;
            }
            this.firstLogin = false;
            this.name = player.getName();
            this.firstLoginMillis = document.getLong(column_firstLoginTime);
            this.lastLoginMillis = document.getLong(column_lastLoginTime);
            this.lastLogoutMillis = document.getLong(column_lastLogoutTime);
            this.onlineTimeMillis = document.getLong(column_onlineTime);
            this.inventoryContent = document.getString(column_inventoryContent);
            this.health = document.getDouble(column_playerHealth);
            this.food = document.getInteger(column_playerFoodLevel);
            this.gameMode = GameMode.valueOf(document.getString(column_playerGamemode));
        });
    }

    public void savePlayer() {
        Document document = new Document(column_lastName, UUIDFetcher.getName(getUuid()))
                .append(column_firstLoginTime, getFirstLoginMillis())
                .append(column_lastLoginTime, getLastLoginMillis())
                .append(column_lastLogoutTime, getLastLogoutMillis())
                .append(column_onlineTime, getOnlineTimeMillis())
                .append(column_inventoryContent, InventoryUtil.itemStackArrayToBase64(player.getInventory().getContents()))
                .append(column_playerHealth, this.player.getHealth())
                .append(column_playerFoodLevel, this.player.getFoodLevel())
                .append(column_playerGamemode, this.player.getGameMode().name());
        Document document1 = new Document("$set", document);
        MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInfo().updateOne(Filters.eq("UUID", this.uuid), document1, (updateResult, throwable) -> {System.out.println("Das Inventar von " + player.getName() + " wurde gespeichert!");});
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public boolean isFirstLogin() {
        return firstLogin;
    }

    public long getFirstLoginMillis() {
        return firstLoginMillis;
    }

    public long getLastLoginMillis() {
        return lastLoginMillis;
    }

    public long getLastLogoutMillis() {
        return lastLogoutMillis;
    }

    public long getOnlineTimeMillis() {
        return onlineTimeMillis;
    }

    public String getInventoryContent() {
        return inventoryContent;
    }

    public double getHealth() {
        return health;
    }

    public int getFood() {
        return food;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public String getColumn_firstLoginTime() {
        return column_firstLoginTime;
    }

    public String getColumn_lastLoginTime() {
        return column_lastLoginTime;
    }

    public String getColumn_inventoryContent() {
        return column_inventoryContent;
    }

    public String getColumn_lastLogoutTime() {
        return column_lastLogoutTime;
    }

    public String getColumn_lastName() {
        return column_lastName;
    }

    public static Player getPlayer() {
        return player;
    }

    public String getColumn_uuid() {
        return column_uuid;
    }

    public String getColumn_onlineTime() {
        return column_onlineTime;
    }

    public String getColumn_playerFoodLevel() {
        return column_playerFoodLevel;
    }

    public String getColumn_playerGamemode() {
        return column_playerGamemode;
    }

    public String getColumn_playerHealth() {
        return column_playerHealth;
    }

    public static void setHealth(double health) {
        UniquePlayer.health = health;
    }

    public void setColumn_lastName(String column_lastName) {
        this.column_lastName = column_lastName;
    }

    public void setColumn_uuid(String column_uuid) {
        this.column_uuid = column_uuid;
    }

    public void setColumn_firstLoginTime(String column_firstLoginTime) {
        this.column_firstLoginTime = column_firstLoginTime;
    }

    public void setColumn_inventoryContent(String column_inventoryContent) {
        this.column_inventoryContent = column_inventoryContent;
    }

    public void setColumn_lastLoginTime(String column_lastLoginTime) {
        this.column_lastLoginTime = column_lastLoginTime;
    }

    public static void setFood(int food) {
        UniquePlayer.food = food;
    }

    public void setColumn_lastLogoutTime(String column_lastLogoutTime) {
        this.column_lastLogoutTime = column_lastLogoutTime;
    }

    public void setColumn_onlineTime(String column_onlineTime) {
        this.column_onlineTime = column_onlineTime;
    }

    public void setColumn_playerFoodLevel(String column_playerFoodLevel) {
        this.column_playerFoodLevel = column_playerFoodLevel;
    }

    public void setColumn_playerGamemode(String column_playerGamemode) {
        this.column_playerGamemode = column_playerGamemode;
    }

    public void setColumn_playerHealth(String column_playerHealth) {
        this.column_playerHealth = column_playerHealth;
    }

    public static void setFirstLogin(boolean firstLogin) {
        UniquePlayer.firstLogin = firstLogin;
    }

    public static void setFirstLoginMillis(long firstLoginMillis) {
        UniquePlayer.firstLoginMillis = firstLoginMillis;
    }

    public static void setGameMode(GameMode gameMode) {
        UniquePlayer.gameMode = gameMode;
    }

    public static void setPlayer(Player player) {
        UniquePlayer.player = player;
    }

    public static void setInventoryContent(String inventoryContent) {
        UniquePlayer.inventoryContent = inventoryContent;
    }

    public static void setLastLoginMillis(long lastLoginMillis) {
        UniquePlayer.lastLoginMillis = lastLoginMillis;
    }

    public static void setLastLogoutMillis(long lastLogoutMillis) {
        UniquePlayer.lastLogoutMillis = lastLogoutMillis;
    }

    public static void setName(String name) {
        UniquePlayer.name = name;
    }

    public static void setOnlineTimeMillis(long onlineTimeMillis) {
        UniquePlayer.onlineTimeMillis = onlineTimeMillis;
    }

    public static void setUuid(UUID uuid) {
        UniquePlayer.uuid = uuid;
    }
}
