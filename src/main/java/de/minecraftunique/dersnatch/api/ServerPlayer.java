package de.minecraftunique.dersnatch.api;

import com.mongodb.client.model.Filters;
import de.minecraftunique.dersnatch.MinecraftUniqueServerAPI;
import de.minecraftunique.dersnatch.util.InventoryUtil;
import de.minecraftunique.dersnatch.util.ItemBuilder;
import de.minecraftunique.dersnatch.util.uuid.UUIDFetcher;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ServerPlayer {

    /**private UUID uuid;

    private String name;
    private boolean isFirstLogin;
    private long firstLoginTime;
    private long lastLogoutTime;

    public ServerPlayer(Player player) {
        this.uuid = UUIDFetcher.getUUID(player.getName());
        MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInfo().find(new Document("UUID", uuid)).first((d1, t1) -> {
            if (d1 == null) {
                this.isFirstLogin = true;
                load(UUIDFetcher.getName(uuid), new ServerPlayerInventory(getDefaultItems(), 20.0, 15, 1.5), System.currentTimeMillis(), Long.valueOf("0"));
                return;
            }
            this.isFirstLogin = false;
            MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInventory().find(new Document("UUID", uuid)).first((d2, t2) -> {
                load(
                        UUIDFetcher.getName(uuid),
                        new ServerPlayerInventory(d2.getString("Content"),
                                d2.getDouble("Health"),
                                d2.getInteger("Food"),
                                d2.getDouble("Level"),
                                GameMode.valueOf(d2.getString("Gamemode"))),
                        d1.getLong("FirstLoginTime"),
                        d1.getLong("LastLogoutTime"));
                System.out.println("Load! " + d2.getString("Content"));
            });
        });
    }

    private void load(String name, ServerPlayerInventory serverPlayerInventory, long firstLoginTime, long lastLogoutTime) {
        this.name = name;
        this.serverPlayerInventory = serverPlayerInventory;
        this.firstLoginTime = firstLoginTime;
        this.lastLogoutTime = lastLogoutTime;
    }

    private ItemStack[] getDefaultItems() {
        List<ItemStack> itemStacks = new ArrayList<>();
        itemStacks.add(new ItemBuilder(Material.LEATHER_HELMET).getItemStack());
        itemStacks.add(new ItemBuilder(Material.LEATHER_CHESTPLATE).getItemStack());
        itemStacks.add(new ItemBuilder(Material.LEATHER_LEGGINGS).getItemStack());
        itemStacks.add(new ItemBuilder(Material.LEATHER_BOOTS).getItemStack());
        return itemStacks.toArray(new ItemStack[0]);
    }

    private void debug(String s) {
        System.out.println(s);
    }

    public String getName() {
        return name;
    }

    public ServerPlayerInventory getServerPlayerInventory() {
        return serverPlayerInventory;
    }

    public long getFirstLoginTime() {
        return firstLoginTime;
    }

    public boolean isFirstLogin() {
        return isFirstLogin;
    }

    public long getLastLogoutTime() {
        return lastLogoutTime;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void savePlayer() {
        Player player = Bukkit.getPlayer(getUuid());
        player.sendMessage("Test");
        debug("Speichere Daten von " + player.getName() + " - " + getUuid());
        Document d1 = new Document("Name", player.getName())
                .append("FirstLoginTime", getFirstLoginTime())
                .append("LastLogoutTime", System.currentTimeMillis());
        Document d2 = new Document("Content", InventoryUtil.itemStackArrayToBase64(player.getInventory().getContents()))
                .append("Health", player.getHealth())
                .append("Food", player.getFoodLevel())
                .append("Level", Double.valueOf(player.getExpToLevel()))
                .append("Gamemode", player.getGameMode().name());
        if (isFirstLogin()) {
            MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInventory().insertOne(new Document("UUID", getUuid()), (aVoid, throwable) -> {});
            MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInfo().insertOne(new Document("UUID", getUuid()), (aVoid, throwable) -> {});
        }
        MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInventory().updateOne(Filters.eq("UUID", UUIDFetcher.getUUID(player.getName())), new Document("$set", d2), (updateResult, throwable) -> {});
        MinecraftUniqueServerAPI.getInstance().getMongoManager().getPlayerInfo().updateOne(Filters.eq("UUID", UUIDFetcher.getUUID(player.getName())), new Document("$set", d1), (updateResult, throwable) -> {});
    }*/
}
