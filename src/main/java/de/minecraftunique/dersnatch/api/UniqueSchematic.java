package de.minecraftunique.dersnatch.api;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import org.bukkit.Location;

import java.io.File;
import java.io.IOException;

public class UniqueSchematic {

    public UniqueSchematic(Location location, File file) {
        try {
            Schematic schematic = FaweAPI.load(file);
            EditSession editSession = schematic.paste(new BukkitWorld(location.getWorld()), BukkitUtil.toVector(location));
            editSession.setFastMode(true);
            editSession.flushQueue();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
